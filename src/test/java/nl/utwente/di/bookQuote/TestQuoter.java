package nl.utwente.di.bookQuote;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class TestQuoter {

    @Test
    public void testBook1() {
        Quoter quoter = new Quoter();
        double price = quoter.getBookPrice("1");
        Assertions.assertEquals(10.0, price, 0.0, "Price of Book 1");
    }

    @Test
    public void testBook2() {
        Quoter quoter = new Quoter();
        double price = quoter.getBookPrice("2");
        Assertions.assertEquals(45.0, price, 0.0, "Price of Book 2");
    }

    @Test
    public void testBook3() {
        Quoter quoter = new Quoter();
        double price = quoter.getBookPrice("3");
        Assertions.assertEquals(20.0, price, 0.0, "Price of Book 3");
    }

    @Test
    public void testBook4() {
        Quoter quoter = new Quoter();
        double price = quoter.getBookPrice("4");
        Assertions.assertEquals(35.0, price, 0.0, "Price of Book 4");
    }

    @Test
    public void testBook5() {
        Quoter quoter = new Quoter();
        double price = quoter.getBookPrice("5");
        Assertions.assertEquals(50.0, price, 0.0, "Price of Book 5");
    }

    @Test
    public void testBook99() {
        Quoter quoter = new Quoter();
        double price = quoter.getBookPrice("99");
        Assertions.assertEquals(0.0, price, 0.0, "Price of Book 99");
    }

}